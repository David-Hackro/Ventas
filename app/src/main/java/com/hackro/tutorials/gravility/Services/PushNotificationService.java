package com.hackro.tutorials.gravility.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;
import com.hackro.tutorials.gravility.Activities.Splash;
import com.hackro.tutorials.gravility.R;

import java.util.Random;

/**
 * Created by kundan on 10/22/2015.
 */
public class PushNotificationService extends GcmListenerService{

    @Override
    public void onMessageReceived(String from, Bundle data) {
        //Obtenemos los datos que son enviados del servicio
        int id = data.getInt("actionParam");
        String message = data.getString("message");
        String  title= data.getString("title");
        String subtitle = data.getString("subtitle");
        String ticketText = data.getString("ticketText");
        String largeIcon= data.getString("largeIcon");
        String smallIcon = data.getString("smallIcon");


        //createNotification(mTitle, push_msg);
        showNotification(message,message,message,id);
    }


//Creacion de la notificacion
    public void showNotification(String title,String message,String ticketText,int id) {

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        Intent notificationIntent = new Intent(this, Splash.class);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.anuncio)
                .setAutoCancel(true)
                .setColor(getResources().getColor(R.color.colorAccent))
                .setPriority(2)
                .setLargeIcon(bm)
                .setTicker(title)
                .setContentText(message)
                .setContentIntent(PendingIntent.getActivity(this, 0, notificationIntent, 0))
                .setWhen(System.currentTimeMillis())
                .setContentTitle(ticketText)
                .setDefaults(Notification.DEFAULT_ALL);

        Notification notification = builder.build();
        //Para poder tener multiples notificaciones generamos un random
        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;
        ((NotificationManager) this.getSystemService(NOTIFICATION_SERVICE)).notify(m, notification);


    }
}
