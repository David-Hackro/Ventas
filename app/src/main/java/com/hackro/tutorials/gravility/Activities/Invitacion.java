package com.hackro.tutorials.gravility.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.hackro.tutorials.gravility.R;
import com.hackro.tutorials.gravility.Services.Services;
import com.hackro.tutorials.gravility.Utilidades.Utilidades;

public class Invitacion extends AppCompatActivity {

    private Utilidades utilidades;
    private EditText Email;
    private Services service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitacion);
        utilidades = new Utilidades(Invitacion.this);
        Email = (EditText) findViewById(R.id.txtEmail);
        service = new Services();
    }


    public void Invitar(View view) {
        if (!Email.getText().toString().contains("@gmail.com")) {
            Email.setError("Unicamente permitidos correos @gmail");
        } else {
            service.sendInvitation(utilidades.getEmail(), Email.getText().toString());
            Toast.makeText(Invitacion.this, "Se ha enviado la invitacion", Toast.LENGTH_SHORT).show();
        }
    }

}
