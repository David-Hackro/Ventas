package com.hackro.tutorials.gravility.Utilidades;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by hackro on 18/04/16.
 */
public class Utilidades
{
    private Context context;

    public Utilidades() {
    }

    public Utilidades(Context context) {
        this.context = context;
    }

    public boolean isNetworkAvailable() {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }


    ///Obtener cuenta de g mail ligada al dispositivo
    public String getEmail() {
        AccountManager accountManager = AccountManager.get(context);
        Account[] accounts = accountManager.getAccounts();
        Account account = accounts[0];//getAccount(accountManager);
        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }
}
