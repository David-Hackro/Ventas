package com.hackro.tutorials.gravility.Interfaces;

/**
 * Created by david on 13/04/16.
 */
public interface IService {

    public boolean getAllData();

    public boolean sendInvitation(String email,String invitado);
}
