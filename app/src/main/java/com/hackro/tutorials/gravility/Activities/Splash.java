package com.hackro.tutorials.gravility.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.hackro.tutorials.gravility.Entities.Categoria;
import com.hackro.tutorials.gravility.R;
import com.hackro.tutorials.gravility.Services.GCMClientManager;
import com.hackro.tutorials.gravility.Services.Services;
import com.hackro.tutorials.gravility.Utilidades.Utilidades;

import io.realm.RealmConfiguration;

public class Splash extends AppCompatActivity {

    private ProgressDialog progress;
    private Services service;
    private RealmConfiguration realmConfiguration;
    private Utilidades utilidades;
    private  String PROJECT_NUMBER="710539202415";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        utilidades = new Utilidades(Splash.this);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        progress = ProgressDialog.show(this, null, null, true);
        progress.setContentView(R.layout.elemento_progress_dialog);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        GCMClientManager pushClientManager = new GCMClientManager(this, PROJECT_NUMBER);
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {

                Log.e("Registration id", registrationId);

                //Este id que se genera es el que deberas almacenar para poder enviar las notificaciones a este dispositivo en especifico

            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
            }
        });


        new MyAsyncClass().execute();



    }

    private class MyAsyncClass extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            realmConfiguration = new RealmConfiguration.Builder(Splash.this).build();

            service = new Services(realmConfiguration);
            if (utilidades.isNetworkAvailable()) {
                if (service.getAllData()) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(Splash.this, getResources().getString(R.string.Online), Toast.LENGTH_SHORT).show();
                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(Splash.this, getResources().getString(R.string.Offline), Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            } else {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(Splash.this, getResources().getString(R.string.Offline), Toast.LENGTH_SHORT).show();
                    }
                });

            }
            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            progress.dismiss();

            startActivity(new Intent(Splash.this, MenuDrawer.class));
            finish();

        }
    }

    public RealmConfiguration getRealmConfiguration() {
        return realmConfiguration;
    }

    public void setRealmConfiguration(RealmConfiguration realmConfiguration) {
        this.realmConfiguration = realmConfiguration;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
